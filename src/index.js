const amqp = require("amqplib/callback_api");
const fs = require("fs");
const { exec } = require("child_process");

amqp.connect("amqp://localhost", function(error0, connection) {
  if (error0) {
    throw error0;
  }

  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }

    var queue = "process_job_queue";

    channel.assertQueue(queue, {
      durable: false
    });
    channel.consume(
      queue,
      function(msg) {
        console.log(msg);
        const ids = msg.content.toString().split(" ");
        if (!fs.existsSync(`./jobs/${ids[0]}`)) {
          fs.mkdirSync(`./jobs/${ids[0]}`, { recursive: true });
          exec(`node ./index.js`, (error, stdout, stderr) => {
            if (error) {
              console.log(`error: ${error.message}`);
              return;
            }
            if (stderr) {
              console.log(`stderr: ${stderr}`);
              return;
            }
            console.log(`stdout: ${stdout}`);
          });
        }

        fs.writeFileSync(`./jobs/${ids[0]}/${ids[1]}.xml`);
      },
      {
        noAck: true
      }
    );
  });
});
